import React from "react";
import { useMediaQuery } from 'react-responsive';

export default function Infotoko() {
    const isDesktopOrLaptop = useMediaQuery({
        query: '(min-device-width: 1224px)'
      })
    return (
        <div> {isDesktopOrLaptop ? (
        <p>
            <div class="v-main__wrap"
                style={{
                    flex: "1, 1, auto",
                    maxWidth: "100%",
                    position: "relative"
                }}
            >
                <div class="container"
                    style={{
                        padding: 0,
                        width: "100%",
                        marginRight: "auto",
                        marginLeft: "auto"
                    }}
                >
                    <div class="pa-page"
                        style={{
                            paddingLeft: "375px",
                            paddingRight: "24px",
                            paddingBottom: "78px"
                        }}
                    >
                        <div class="display-2"
                            style={{
                                fontWeight: 500,
                                lineHeight: "24px",
                                letterSpacing: 0,
                                fontFamily: "Roboto,sans-serif",
                                fontSize: "20px",
                                wordBreak: "break-word"
                            }}
                        >
                            KodingWorks
                        </div>
                    </div>
                </div>
            </div>
        </p> 
        ) : (
        <p>
            <div class="v-main__wrap"
                style={{
                    flex: "1, 1, auto",
                    maxWidth: "100%",
                    position: "relative"
                }}
            >
                <div class="container"
                    style={{
                        padding: 0,
                        width: "100%",
                        marginRight: "auto",
                        marginLeft: "auto"
                    }}
                >
                    <div class="pa-page"
                        style={{
                            paddingLeft: "24px",
                            paddingRight: "24px",
                            paddingBottom: "80px"
                        }}
                    >
                        <div class="display-2"
                            style={{
                                fontWeight: 500,
                                lineHeight: "24px",
                                letterSpacing: 0,
                                fontFamily: "Roboto,sans-serif",
                                fontSize: "20px",
                                wordBreak: "break-word"
                            }}
                        >
                            KodingWorks
                        </div>
                    </div>
                </div>
            </div>
        </p>)
        } 
        </div>
    )
}