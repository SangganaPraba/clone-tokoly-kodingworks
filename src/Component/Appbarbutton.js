import React from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Back from "../Image/back.svg";
import Button2 from "../Image/button2.svg";
import Button3 from "../Image/button3.svg";
import { useMediaQuery } from 'react-responsive';

function ElevationScroll(props) {
  const { children, window } = props;
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
    target: window ? window() : undefined,
  });

  return React.cloneElement(children, {
    elevation: trigger ? 0 : 0,
  });
}

ElevationScroll.propTypes = {
  children: PropTypes.element.isRequired,
  window: PropTypes.func,
};

export default function Appbar(props) {
  const isDesktopOrLaptop = useMediaQuery({
    query: '(min-device-width: 1224px)'
  })
  return (
    <div> {isDesktopOrLaptop ? (
      <React.Fragment>
      <ElevationScroll {...props}>
        <AppBar color="white">
          <Toolbar>
            <div
              style={{
                display: "flex",
                width: "100%",
                justifyContent: "center"
              }}
            >
              <div
                style={{
                  alignSelf: "center"
                }}
              >
                <Typography
                  class="flex text-strong lines-1 clickable"
                  style={{
                    cursor: "pointer",
                    whiteSpace: "nowrap",
                    overflow: "hidden",
                    textOverflow: "ellipsis",
                    fontSize: "16px",
                    lineHeight: "18px",
                    fontWeight: 500,
                    flex: "1, 1, auto",
                    maxWidth: "100%",
                    padding: 0,
                    margin: 0,
                    color: "rgba(0,0,0,.8)",
                    fontFamily: "Roboto,sans-serif",
                  }}>
                  <b>KodingWorks</b>
                </Typography>
              </div>
              <div
                style={{
                  position: "absolute",
                  right: 8,
                  top: 8,
                  display: "flex",
                  alignSelf: "center",
                }}
              >
                <Button
                  type="button"
                  class="app-bar__btn v-btn--overflow v-btn v-btn--flat v-btn--icon v-btn--round v-btn--text theme--light v-size--default"
                  style={{
                    height: "48px",
                    width: "48px",
                    color: "rgba(0,0,0,.8)",
                    cursor: "pointer",
                    borderRadius: "50%",
                    backgroundColor: "transparent",
                    alignItems: "center",
                    display: "inline-flex",
                    flex: "0, 0, auto",
                    justifyContent: "center",
                    outline: 0,
                    position: "relative",
                    transitionDuration: "28s",
                    borderStyle: "none"
                  }}
                >
                  <img src={Button2}></img>
                </Button>
                <Button
                  type="button"
                  class="app-bar__btn v-btn--overflow ml-2 v-btn v-btn--flat v-btn--icon v-btn--round v-btn--router v-btn--text theme--light v-size--default v-btn--light"
                  style={{
                    height: "48px",
                    width: "48px",
                    color: "rgba(0,0,0,.8)",
                    marginLeft: "8px",
                    cursor: "pointer",
                    background: "rgba(0,0,0,.08)",
                    borderRadius: "50%",
                    alignItems: "center",
                    display: "inline-flex",
                    flex: "0, 0, auto",
                    justifyContent: "center",
                    outline: 0,
                    position: "relative",
                    transitionDuration: "28s",
                    borderStyle: "none",
                  }}
                >
                  <img src={Button3}></img>
                </Button>
              </div>
            </div>
          </Toolbar>
        </AppBar>
      </ElevationScroll>
      <Toolbar />
    </React.Fragment>
    ) : (
      <React.Fragment>
      <ElevationScroll {...props}>
        <AppBar color="white">
          <Toolbar>
            <header class="v-app-bar--reverse v-sheet theme--light v-toolbar v-toolbar--flat v-app-bar v-app-bar--clipped v-app-bar--fixed"
              style={{
                height: "66px",
                marginTop: "5px",
                transform: "translateY(0px)",
                left: "8px",
                right: "0px",
                borderRadius: "0px",
                backgroundColor: "#FFFFFF",
                display: "flex",
                position: "fixed",
                top: 0,
                zIndex: 5,
                borderColor: "#fff",
                maxWidth: "100%",
                transition: "transform 2s"
              }}
            >
            <Button
              type="button"
              class="v-btn--light mr-2 v-btn v-btn--flat v-btn--icon v-btn--round theme--light v-size--default"
              style={{
                height: "48px",
                width: "48px",
                color: "rgba(0,0,0,.8)",
                marginRight: "8px",
                background: "rgba(0,0,0,.08)",
                cursor: "pointer",
                borderRadius: "50%",
                alignItems: "center",
                display: "inline-flex",
                justifyContent: "center",
                outline: 0,
                position: "relative",
                transitionDuration: "28s",
                borderStyle: "none"
              }}
            >
              <img src={Back}></img>
            </Button>
              <Typography
                class="flex text-strong lines-1 clickable"
                style={{
                  cursor: "pointer",
                  whiteSpace: "nowrap",
                  overflow: "hidden",
                  textOverflow: "ellipsis",
                  fontSize: "16px",
                  fontWeight: 500,
                  maxWidth: "100%",
                  padding: 0,
                  margin: 0,
                  color: "rgba(0,0,0,.8)",
                  fontFamily: "Roboto,sans-serif",
                  marginTop: "15px"
                }}
              >
                  <b>KodingWorks</b>
                </Typography>
                <div
                  style={{
                    position: "absolute",
                    right: 8,
                    top: 0,
                    display: "flex",
                    alignSelf: "flex-start",
                  }}
                >
                  <Button
                    type="button"
                    class="app-bar__btn v-btn--overflow v-btn v-btn--flat v-btn--icon v-btn--round v-btn--text theme--light v-size--default"
                    style={{
                      height: "48px",
                      width: "48px",
                      color: "rgba(0,0,0,.8)",
                      cursor: "pointer",
                      borderRadius: "50%",
                      backgroundColor: "transparent",
                      alignItems: "center",
                      display: "inline-flex",
                      justifyContent: "center",
                      outline: 0,
                      position: "relative",
                      transitionDuration: "28s",
                      borderStyle: "none"
                    }}
                  >
                    <img src={Button2}></img>
                  </Button>
                  <Button
                    type="button"
                    class="app-bar__btn v-btn--overflow ml-2 v-btn v-btn--flat v-btn--icon v-btn--round v-btn--router v-btn--text theme--light v-size--default v-btn--light"
                    style={{
                      height: "48px",
                      width: "48px",
                      color: "rgba(0,0,0,.8)",
                      marginLeft: "8px",
                      cursor: "pointer",
                      background: "rgba(0,0,0,.08)",
                      borderRadius: "50%",
                      alignItems: "center",
                      display: "inline-flex",
                      flex: "0, 0, auto",
                      justifyContent: "center",
                      outline: 0,
                      position: "relative",
                      transitionDuration: "28s",
                      borderStyle: "none",
                    }}
                  >
                    <img src={Button3}></img>
                  </Button>
                </div>
            </header>
          </Toolbar>
        </AppBar>
      </ElevationScroll>
      <Toolbar />
    </React.Fragment>
    )} 
    </div>
    
  );
}
