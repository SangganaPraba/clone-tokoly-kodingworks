import React from "react";
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { Link } from "react-router-dom";

export default function Cardproduk() {
  
  return (
    <div>
      <Paper elevation={2}
        style={{
          borderRadius: "8px",
          backgroundColor: "#fff",
          color: "rgba(0,0,0,.8)",
          borderColor: "#fff",
          cursor: "pointer",
          display: "block",
          textDecoration: "none",
          transitionProperty: "box-shadow, opacity",
          position: "relative",
          marginRight: 25,
        }}
          component={Link}
          to="/iphone"
      >
        <img src="https://dbs9nopbkp043.cloudfront.net/images/products/119229100_323814172178042_4355931992402697069_n_1612839908143_resized512.jpg"
          style={{
            backgroundPosition: "center center",
            backgroundColor: "rgba(0,0,0,.1)",
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
            top: 0,
            left: 0,
            width: 325
          }}
        />
        <div class="layout overlay fill-height column pa-1 justify-space-between no-click"
          style={{
            zIndex: 1,
            padding: "4px",
            justifyContent: "space-between",
            flexDirection: "column",
            pointerEvents: "none",
            position: "absolute",
            top: "64%",
            display: "flex",
            flex: "1, 1, auto",
            flexWrap: "nowrap"
          }}
        >
        <div class="layout product-top-labels wrap shrink justify-end"
          style={{
            justifyContent: "flex-end",
            flexWrap: "wrap",
            flexGrow: 0,
            flexShrink: 1,
            display: "flex",
            flex: "1, 1, auto"
          }}
        >
          <div class="layout product-labels wrap shrink"
            style={{
              flexWrap: "wrap",
              flexGrow: 0,
              flexShrink: 1,
              display: "flex",
              flex: "1, 1, auto"
            }}
          >
            <div class="badge secondary small"
              style={{
                fontSize: "12px",
                margin: "4px, 4px, 0, 0",
                color: "#fff",
                padding: "2px",
                verticalAlign: "baseline",
                display: "inline-flex",
                alignItems: "center",
                backgroundColor: "#000",
                borderColor: "#000",
                fontFamily: "Roboto,sans-serif"
              }}
            >
              Ada Stok
            </div>
          </div>
        </div>
        </div>
          <div class="layout pa-2 column"
            style={{
              padding: "6px"
            }}
          >
            <Typography class="flex display-1 lines-1 flex--wrap"
              style={{
                fontSize: "15px",
                fontWeight: 500,
                fontFamily: "Roboto,sans-serif",
                margin: 0,
                color: "rgba(0,0,0,.8)"
              }}
            >
              <b>Iphone</b>
            </Typography>
          </div>
          <div class="flex light--text flex--wrap"
            style={{
              color: "#9f9f9f",
              caretColor: "#9f9f9f",
              flex: "0, 0, auto"
            }}
          >
            <Typography class="lines-1"
              style={{
                fontFamily: "Roboto,sans-serif",
                fontSize: "15px",
                paddingLeft: "6px",
                margin: 0,
                marginBottom: "60px"
              }}
            >
              Rp 15,000,000
            </Typography>
          </div>
          <Typography class="flex text-center flex--wrap"
            style={{
              textAlign: "center",
              flex: "0, 0, auto",
              color: "rgba(0,0,0,.8)",
              fontFamily: "Roboto,sans-serif",
              fontSize: "16px",
              paddingBottom: "10px"
            }}
          >
            Lihat Produk
          </Typography>
      </Paper>
    </div>
  );
}