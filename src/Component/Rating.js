import React from "react";
import Grid from '@material-ui/core/Grid';
import Bintang from "../Image/bintang.svg";
import { useMediaQuery } from 'react-responsive';

export default function Rating() {
    const isDesktopOrLaptop = useMediaQuery({
        query: '(min-device-width: 1224px)'
      })
    return (
        <div> {isDesktopOrLaptop ? (
            <div class="layout faded column pb-10"
                style={{
                    height: "100%",
                    flex: "1, 1, auto",
                    paddingBottom: "40.5%",
                    flexDirection: "column",
                    backgroundColor: "rgba(0,0,0,.08)",
                    display: "flex",
                    flexWrap: "nowrap"
                }}
            >
            <div class="bg page-header--wrapper"
                style={{
                    backgroundColor: "#FFFFFF"
                }}
            >
                <div class="px-page pt-6 pb-4 page-width page-width--small"
                    style={{
                        maxWidth: "608px",
                        paddingBottom: "16px",
                        paddingTop: "22px",
                        width: "100%",
                        marginLeft: "auto",
                        marginRight: "auto",
                        paddingLeft: "24px",
                        paddingRight: "24px"
                    }}
                >
                    <Grid container spacing={3}>
                        <Grid item xs={3}>
                            <div class="layout font-weight-bold align-center"
                                style={{
                                    height: "20px",
                                    alignItems: "center",
                                    fontWeight: 700,
                                    marginBottom: 2
                                }}
                            >
                                0
                            </div>
                            <div class="caption light--text"
                                style={{
                                    fontSize: "14px",
                                    fontWeight: 400,
                                    letterSpacing: 0,
                                    fontFamily: "Roboto,sans-serif",
                                    color: "#9f9f9f",
                                    caretColor: "#9f9f9f"
                                }}
                            >
                                Testimoni
                            </div>
                        </Grid>
                        <Grid item xs={4}>
                            <div class="layout font-weight-bold align-center"
                                style={{
                                    height: "20px",
                                    alignItems: "center",
                                    fontWeight: 700,
                                    marginBottom: 2,
                                    marginLeft: 45
                                }}
                            >
                                0
                            </div>
                            <div class="caption light--text"
                                style={{
                                    fontSize: "14px",
                                    fontWeight: 400,
                                    letterSpacing: 0,
                                    fontFamily: "Roboto,sans-serif",
                                    color: "#9f9f9f",
                                    caretColor: "#9f9f9f",
                                    marginLeft: 45
                                }}
                            >
                                Rating
                            </div>
                        </Grid>
                        <Grid item xs={3}>
                            <div class="layout font-weight-bold align-center"
                                style={{
                                    height: "20px",
                                    alignItems: "center",
                                    fontWeight: 700,
                                    display: "flex",
                                    marginBottom: 2,
                                    marginLeft: 37
                                }}
                            >
                                <span class="mr-1"
                                    style={{
                                        marginRight: "4px",
                                        marginBottom: 2
                                    }}
                                >
                                    0
                                </span>
                                    <div class="layout"
                                        style={{
                                            margin: "0px, -1px",
                                            height: "14px",
                                            display: "flex",
                                            flexWrap: "nowrap"
                                        }}
                                    >
                                        <div class="flex"
                                            style={{
                                                width: "14px",
                                                maxWidth: "14px"
                                            }}
                                        >
                                            <img src={Bintang}
                                                style={{
                                                    width: "100%",
                                                    height: "100%"
                                                }}
                                            >
                                            </img>
                                        </div>
                                        <div class="flex"
                                            style={{
                                                width: "14px",
                                                maxWidth: "14px"
                                            }}
                                        >
                                            <img src={Bintang}
                                                style={{
                                                    width: "100%",
                                                    height: "100%",
                                                    marginLeft: 3
                                                }}
                                            >
                                            </img>
                                        </div>
                                        <div class="flex"
                                            style={{
                                                width: "14px",
                                                maxWidth: "14px"
                                            }}
                                        >
                                            <img src={Bintang}
                                                style={{
                                                    width: "100%",
                                                    height: "100%",
                                                    marginLeft: 4
                                                }}
                                            >
                                            </img>
                                        </div>
                                        <div class="flex"
                                            style={{
                                                width: "14px",
                                                maxWidth: "14px"
                                            }}
                                        >
                                            <img src={Bintang}
                                                style={{
                                                    width: "100%",
                                                    height: "100%",
                                                    marginLeft: 5
                                                }}
                                            >
                                            </img>
                                        </div>
                                        <div class="flex"
                                            style={{
                                                width: "14px",
                                                maxWidth: "14px"
                                            }}
                                        >
                                            <img src={Bintang}
                                                style={{
                                                    width: "100%",
                                                    height: "100%",
                                                    marginLeft: 6
                                                }}
                                            >
                                            </img>
                                        </div>
                                    </div>
                            
                            </div>
                                <div class="caption light--text"
                                        style={{
                                            fontSize: "14px",
                                            fontWeight: 400,
                                            letterSpacing: 0,
                                            fontFamily: "Roboto,sans-serif",
                                            color: "#9f9f9f",
                                            caretColor: "#9f9f9f",
                                            flexWrap: "nowrap",
                                            marginLeft: 37
                                        }}
                                    >
                                        Rata-Rata
                                    </div>
                        </Grid>
                    </Grid>
                </div>
            </div>
        </div>
        ) : (
        <div class="v-main__wrap"
            style={{
                maxWidth: "100%",
                position: "relative"
            }}
        >
            <div class="container container--fluid fill-height"
                style={{
                    padding: 0,
                    alignItems: "center",
                    display: "flex",
                    flexWrap: "wrap",
                    maxWidth: "100%",
                    width: "100%",
                    marginRight: "auto",
                    marginLeft: "auto",
                    height: "100%"
                }}
            >
                <div class="layout faded column pb-10"
                style={{
                    height: "100%",
                    paddingBottom: "140%",
                    paddingRight: "27%",
                    flexDirection: "column",
                    backgroundColor: "rgba(0,0,0,.08)",
                    display: "flex",
                    flexWrap: "nowrap"
                }}
            >
                <div class="bg page-header--wrapper"
                    style={{
                        backgroundColor: "#FFFFFF"
                    }}
                >
                    <div class="px-page pt-6 pb-4 page-width page-width--small"
                        style={{
                            maxWidth: "100%",
                            paddingBottom: "16px",
                            paddingTop: "24px",
                            width: "100%",
                            marginLeft: "auto",
                            marginRight: "auto",
                            paddingLeft: "24px",
                            paddingRight: "24px"
                        }}
                    >
                        <div class="layout"
                            style={{
                                display: "flex",
                                flexWrap: "nowrap"
                            }}
                        >
                            <div class="flex xs4"
                                style={{
                                    flexBasis: "33.3333333333%",
                                    flexGrow: 0,
                                    maxWidth: "33.3333333333%"
                                }}
                            >
                                <div class="layout font-weight-bold align-center"
                                    style={{
                                        height: "20px",
                                        alignItems: "center",
                                        fontWeight: 500,
                                        display: "flex",
                                        flexWrap: "nowrap",
                                        marginBottom: 2
                                    }}
                                >
                                    0
                                </div>
                                <div class="caption light--text"
                                    style={{
                                        fontSize: "12px",
                                        lineHeight : "16px",
                                        fontWeight: 400,
                                        letterSpacing: 0,
                                        fontFamily: "Roboto,sans-serif",
                                        color: "#9f9f9f",
                                        caretColor: "#9f9f9f"
                                    }}
                                >
                                    Testimoni
                                </div>
                                </div>
                                <div class="flex xs4"
                                    style={{
                                        flexBasis: "33.3333333333%",
                                        flexGrow: 0,
                                        maxWidth: "33.3333333333%"
                                    }}
                                >
                                <div class="layout font-weight-bold align-center"
                                    style={{
                                        height: "20px",
                                        alignItems: "center",
                                        fontWeight: 500,
                                        display: "flex",
                                        flexWrap: "nowrap",
                                        marginBottom: 2,
                                        marginLeft: 15
                                    }}
                                >
                                    0
                                </div>
                                <div class="caption light--text"
                                    style={{
                                        fontSize: "12px",
                                        lineHeight : "16px",
                                        fontWeight: 400,
                                        letterSpacing: 0,
                                        fontFamily: "Roboto,sans-serif",
                                        color: "#9f9f9f",
                                        caretColor: "#9f9f9f",
                                        marginLeft: 15
                                    }}
                                >
                                    Rating
                                </div>
                                </div>
                                <div class="flex xs4"
                                    style={{
                                        flexBasis: "33.3333333333%",
                                        flexGrow: 0,
                                        maxWidth: "33.3333333333%"
                                    }}
                                >
                                    <div class="layout font-weight-bold align-center"
                                    style={{
                                        height: "20px",
                                        alignItems: "center",
                                        fontWeight: 500,
                                        display: "flex",
                                        flexWrap: "nowrap",
                                        marginBottom: 2,
                                        marginLeft: 32
                                    }}
                                >
                                    <span class="mr-1"
                                        style={{
                                            marginRight: "4px"
                                        }}
                                    >
                                        0
                                    </span>
                                        <div class="layout"
                                            style={{
                                                margin: "0px, -1px",
                                                height: "14px",
                                                display: "flex",
                                                flexWrap: "nowrap"
                                            }}
                                        >
                                            <div class="flex"
                                                style={{
                                                    width: "14px",
                                                    maxWidth: "14px"
                                                }}
                                            >
                                                <img src={Bintang}
                                                    style={{
                                                        width: "100%",
                                                        height: "100%"
                                                    }}
                                                >
                                                </img>
                                            </div>
                                            <div class="flex"
                                                style={{
                                                    width: "14px",
                                                    maxWidth: "14px"
                                                }}
                                            >
                                                <img src={Bintang}
                                                    style={{
                                                        width: "100%",
                                                        height: "100%",
                                                        marginLeft: 3
                                                    }}
                                                >
                                                </img>
                                            </div>
                                            <div class="flex"
                                                style={{
                                                    width: "14px",
                                                    maxWidth: "14px"
                                                }}
                                            >
                                                <img src={Bintang}
                                                    style={{
                                                        width: "100%",
                                                        height: "100%",
                                                        marginLeft: 4
                                                    }}
                                                >
                                                </img>
                                            </div>
                                            <div class="flex"
                                                style={{
                                                    width: "14px",
                                                    maxWidth: "14px"
                                                }}
                                            >
                                                <img src={Bintang}
                                                    style={{
                                                        width: "100%",
                                                        height: "100%",
                                                        marginLeft: 5
                                                    }}
                                                >
                                                </img>
                                            </div>
                                            <div class="flex"
                                                style={{
                                                    width: "14px",
                                                    maxWidth: "14px"
                                                }}
                                            >
                                                <img src={Bintang}
                                                    style={{
                                                        width: "100%",
                                                        height: "100%",
                                                        marginLeft: 6
                                                    }}
                                                >
                                                </img>
                                            </div>
                                        </div>
                                
                                </div>
                                    <div class="caption light--text"
                                            style={{
                                                fontSize: "12px",
                                                lineHeight : "16px",
                                                fontWeight: 400,
                                                letterSpacing: 0,
                                                fontFamily: "Roboto,sans-serif",
                                                color: "#9f9f9f",
                                                caretColor: "#9f9f9f",
                                                flexWrap: "nowrap",
                                                marginLeft: 32
                                            }}
                                        >
                                            Rata-Rata
                                        </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        )} 
        </div>
    )
}