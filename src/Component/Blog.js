import React from "react";
import Card from '@material-ui/core/Card';

export default function Blog() {
    return (
        <div class="v-main__wrap"
            style={{
                maxWidth: "100%",
                position: "relative"
            }}
        >
            <div class="container container--fluid"
                style={{
                    padding: 0,
                    maxWidth: "100%",
                    width: "100%",
                    marginRight: "auto",
                    marginLeft: "auto"
                }}
            >
                <div class="layout faded column fill-height"
                    style={{
                        flexDirection: "column",
                        backgroundColor: "rgba(0,0,0,.08)",
                        height: "100%",
                        display: "flex",
                        flexWrap: "nowrap"
                    }}
                >
                    <div class="bg"
                        style={{
                            backgroundColor: "#FFFFFF",
                        }}
                    >
                        <div class="px-page py-6 page-width page-width--small"
                            style={{
                                maxWidth: "608px",
                                paddingTop: "25px",
                                paddingBottom: "25px",
                                width: "100%",
                                marginLeft: "auto",
                                marginRight: "auto",
                                paddingLeft: "24px",
                                paddingRight: "24px"
                            }}
                        >
                            <div class="display-2 mb-4"
                                style={{
                                    marginBottom: "17px",
                                    fontWeight: 600,
                                    letterSpacing: 0,
                                    fontFamily: "Roboto,sans-serif",
                                    wordBreak: "break-word",
                                    color: "rgba(0,0,0,.8)",
                                    fontSize: "19px"
                                }}
                            >
                                Blog Toko
                            </div>
                            <div
                                style={{
                                    fontWeight: 500,
                                    letterSpacing: 0,
                                    fontFamily: "Roboto,sans-serif",
                                    wordBreak: "break-word",
                                    color: "rgba(0,0,0,.8)",
                                    fontSize: "16px"
                                }}
                            >
                                Temukan event, kabar terbaru, dan promosi terbaru di sini!
                            </div>
                        </div>
                        <div class="pa-page page-width page-width--small"
                            style={{
                                maxWidth: "96.9%",
                                width: "100%",
                                marginLeft: "auto",
                                marginRight: "auto",
                                paddingLeft: "24px",
                                paddingRight: "24px",
                                paddingTop: "16px",
                                paddingBottom: "64px",
                                backgroundColor: "rgba(0,0,0,.08)"
                            }}
                        >
                            <Card class="flat-card mt-6 py-6"
                                style={{
                                    width: "41.4%",
                                    padding: "8px, 16px",
                                    background: "rgba(0,0,0,.08)",
                                    borderRadius: "4px",
                                    paddingTop: "24px",
                                    paddingBottom: "24px",
                                    marginTop: "24px",
                                    marginLeft: "29.2%"
                                }}
                            >
                                <div
                                    style={{
                                        color: "rgba(0,0,0,.8)",
                                        fontFamily: "Roboto,sans-serif",
                                        lineHeight: "20px",
                                        fontSize: "16px",
                                        alignItems: "center",
                                        marginLeft: "20px"
                                    }}
                                >
                                    Maaf, belum ada artikel blog.
                                </div>
                            </Card>
                        </div>
                        <div
                            style={{
                                backgroundColor: "#FFFFFF",
                                paddingBottom: "27%"
                            }}
                        >
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}