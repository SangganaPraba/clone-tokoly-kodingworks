import React, { useState, useEffect } from 'react';
import axios from "axios";
import { makeStyles, useTheme } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';
import Container from '@material-ui/core/Container';

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);
const iphoneSteps = [
  {
    imgPath:
      'https://s3-ap-southeast-1.amazonaws.com/tokotalklive/images/products/119229100_323814172178042_4355931992402697069_n_1612839908143.jpg',
  },
  {
    imgPath:
      'https://s3-ap-southeast-1.amazonaws.com/tokotalklive/images/products/119127379_782907115863435_256600212665385810_n_1612839908234.jpg',
  },
  {
    imgPath:
      'https://s3-ap-southeast-1.amazonaws.com/tokotalklive/images/products/119430840_352417315799531_6524208931160960077_n_1612839908343.jpg',
  },
  {
    imgPath:
      'https://s3-ap-southeast-1.amazonaws.com/tokotalklive/images/products/119447002_161740482230201_448985683374464520_n_1612839908439.jpg',
  },
  {
    imgPath:
      'https://s3-ap-southeast-1.amazonaws.com/tokotalklive/images/products/119437428_465742961048330_5318117827186678362_n_1612839908554.jpg',
  },
];
const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 300,
    flexGrow: 1,
  },
  header: {
    display: 'flex',
    alignItems: 'center',
    height: 50,
    paddingLeft: theme.spacing(4),
    backgroundColor: theme.palette.background.default,
  },
  img: {
    height: 200,
    display: 'block',
    maxWidth: 400,
    overflow: 'hidden',
    width: '100%',
  },
}));

function Iphone() {
  const classes = useStyles();
  const theme = useTheme();
  const [activeStep, setActiveStep] = React.useState(0);
  const maxSteps = iphoneSteps.length;
  const [iphones, setIphones] = useState([])
 useEffect(() => {
     axios
       .get("https://api.tokotalk.com/v1/shop/481108/products")
       .then(response => setIphones(response.data));
   }, [])

  const handleStepChange = (step) => {
    setActiveStep(step);
  };
  console.log(iphones);
  return (
    <Container maxWidth="sm">
    <div className={classes.root}>
      <AutoPlaySwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={activeStep}
        onChangeIndex={handleStepChange}
        enableMouseEvents
      >
        {iphones?.data?.map((iphone, index) => (
          <div key={iphone.label}>
            {Math.abs(activeStep - index) <= 2 ? (
              <img className={classes.img} src={iphone.images[0].url} />
            ) : null}
            {iphone?.images?.map(item => <img className={classes.img} src={item.url} />)}
          </div>
        ))}
        
      </AutoPlaySwipeableViews>
      <MobileStepper
        steps={maxSteps}
        position="static"
        variant="dots"
        activeStep={activeStep}
      />
    </div>
    </Container>
  );
}

export default Iphone;
