import React from "react";
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Rating from "../Image/rating.svg";
import Infotoko from "../Image/infotoko.svg";
import Blog from "../Image/blog.svg";
import { Link } from "react-router-dom";

export default function Buttonhomepage() {
    return (
        <>
        <div
            style={{
                marginTop: "24px",
                marginBottom: "24px"
            }}
        >
        <div
            style={{
                maxWidth: "1024px",
                marginLeft: "auto",
                marginRight: "auto",
                justifyContent: "center",
                display: "flex"
            }}
        >
        <div
            style={{
                paddingRight: "18px",
                paddingLeft: "20px"
            }}
        >
        <Button class="mb-2 v-btn--default v-btn v-btn--flat v-btn--icon v-btn--round v-btn--router theme--light v-size--default"
            style={{
                color: "rgba(0,0,0,.8)",
                backgroundColor: "rgba(0,0,0,.08)",
                marginBottom: "8px",
                height: "48px",
                width: "48px",
                cursor: "pointer",
                borderRadius: "50%",
                alignItems: "center",
                display: "inline-flex",
                justifyContent: "center",
                outline: 0,
                position: "relative",
                transitionDuration: "28s",
                userSelect: "none",
                verticalAlign: "middle",
                whiteSpace: "nowrap",
                margin: 0,
                borderStyle: "none"
            }}
                component={Link}
                to="/testimonials"
        >
            <img src={Rating}></img>
        </Button>
        <Typography 
            style={{
                fontFamily: "Roboto,sans-serif",
                lineHeight: "20px",
                fontSize: "16px",
                color: "rgba(0,0,0,.8)",
                marginTop: "8px",
                padding: 0
            }}
        >
            Rating
        </Typography>
        </div>
        <div
            style={{
                paddingRight: "22px",
                paddingLeft: "18px",
                alignItems: "center",
                flexDirection: "column",
                maxWidth: "100%",
                display: "flex"
            }}
        >
        <Button class="mb-2 v-btn--default v-btn v-btn--flat v-btn--icon v-btn--round v-btn--router theme--light v-size--default"
            style={{
                color: "rgba(0,0,0,.8)",
                backgroundColor: "rgba(0,0,0,.08)",
                marginBottom: "8px",
                height: "48px",
                width: "48px",
                cursor: "pointer",
                borderRadius: "50%",
                alignItems: "center",
                display: "inline-flex",
                justifyContent: "center",
                outline: 0,
                position: "relative",
                transitionDuration: "28s",
                userSelect: "none",
                verticalAlign: "middle",
                whiteSpace: "nowrap",
                margin: 0,
                borderStyle: "none"
            }}
                component={Link}
                to="/info"
        >
            <img src={Infotoko}></img>
        </Button>
        <Typography 
            style={{
                fontFamily: "Roboto,sans-serif",
                lineHeight: "20px",
                fontSize: "16px",
                color: "rgba(0,0,0,.8)",
                marginTop: "8px",
                padding: 0
            }}
        >
            Info Toko
        </Typography>
        </div>
        <div
            style={{
                paddingRight: "20px",
                paddingLeft: "16px",
                alignItems: "center",
                flexDirection: "column",
                maxWidth: "100%",
                display: "flex"
            }}
        >
        <Button class="mb-2 v-btn--default v-btn v-btn--flat v-btn--icon v-btn--round v-btn--router theme--light v-size--default"
            style={{
                color: "rgba(0,0,0,.8)",
                backgroundColor: "rgba(0,0,0,.08)",
                marginBottom: "8px",
                height: "48px",
                width: "48px",
                cursor: "pointer",
                borderRadius: "50%",
                alignItems: "center",
                display: "inline-flex",
                justifyContent: "center",
                outline: 0,
                position: "relative",
                transitionDuration: "28s",
                userSelect: "none",
                verticalAlign: "middle",
                whiteSpace: "nowrap",
                margin: 0,
                borderStyle: "none"
            }}
                component={Link}
                to="/news"
        >
            <img src={Blog}></img>
        </Button>
        <Typography 
            style={{
                fontFamily: "Roboto,sans-serif",
                lineHeight: "20px",
                fontSize: "16px",
                color: "rgba(0,0,0,.8)",
                marginTop: "8px",
                padding: 0
            }}
        >
            Blog
        </Typography>
        </div>
        </div>
        </div>
    </>
    )
}