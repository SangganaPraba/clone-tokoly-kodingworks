import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Appbar from "../Component/Appbar";
import Buttonhomepage from "../Component/Buttonhomepage";
import Cardiphone from "../Component/Cardiphone";
import Cardwarung from "../Component/Cardwarung";

export default function Homepage() {
  
    return (
        <>
          <Appbar/>
          <Grid class="o-hidden page-section__content"
            style={{
              maxWidth: "1024px",
              marginLeft: "auto",
              marginRight: "auto",
              overflow: "hidden"
            }}
          >
            <div class="v-responsive swiper-slide p-relative img--cover"
              style={{
                height: "480px",
                flexShrink: 0,
                width: "100%",
                position: "relative",
                transitionProperty: "transform",
                overflow: "hidden",
                flex: "1, 0, auto",
                maxWidth: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <img class="v-responsive__content"
                style={{
                  flex: "1, 0, 0px",
                  maxWidth: "100%",
                  objectFit: "cover",
                  width: "100%",
                  height: "100%",
                  borderStyle: "none",
                  padding: 0,
                  margin: 0,
                  backgroundRepeat: "no-repeat"
                }}
              src="https://d17s3mi54x04ah.cloudfront.net/cover/clothes_resized1024.jpg"
              />
              </div>
              </Grid>
                <Typography class="layout display-3 align-center justify-center"
                    style={{
                      alignItems: "center",
                      justifyContent: "center",
                      fontSize: "24px",
                      lineHeight: "28px",
                      fontWeight: 500,
                      letterSpacing: 0,
                      fontFamily: "Roboto,sans-serif",
                      wordBreak: "break-word",
                      display: "flex",
                      flex: "1, 1, auto",
                      flexWrap: "nowrap",
                      minWidth: 0,
                      textAlign: "center",
                      color: "rgba(0,0,0,.8)"
                    }}>
                        <b> KodingWorks </b>
                </Typography>
                  <Buttonhomepage/>
                
                <div
                    style={{
                      marginTop: "36px",
                      marginBottom: "36px",
                      paddingLeft: "12px"
                    }}
                >
                  <div
                    style={{
                      maxWidth: "1024px",
                      marginLeft: "auto",
                      marginRight: "auto"
                    }}
                  >
                    <div
                      style={{
                        display: "flex",
                        margin: "-4px",
                        padding: "0, 8px"
                      }}
                    >
                      <Cardiphone/>
                      <Cardwarung/>
                    </div>
                  </div>
                </div>
        </>
    )
}