import React from "react";
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Homepage from "../src/Page/Homepage";
import Appbardetail from "./Component/Appbardetail";
import Appbarbutton from "./Component/Appbarbutton";
import Rating from "./Component/Rating";
import Infotoko from "./Component/Infotoko";
import Blog from "./Component/Blog";
import Iphone from "./Component/Iphone";
import Warung from "./Component/Warung";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Homepage/>
        </Route>
        <Route exact path="/iphone">
          <Appbardetail/>
          <Iphone/>
        </Route>
        <Route exact path="/warung">
          <Appbardetail/>
          <Warung/>
        </Route>
        <Route exact path="/testimonials">
          <Appbarbutton/>
          <Rating/>
        </Route>
        <Route exact path="/info">
          <Appbarbutton/>
          <Infotoko/>
        </Route>
        <Route exact path="/news">
          <Appbarbutton/>
          <Blog/>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
